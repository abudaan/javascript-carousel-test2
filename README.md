# A Symfony and Okoa Project

## Dependencies
This project has several dependencies:

* [PHP 5.4+](http://php.net/)
* [Composer](https://getcomposer.org/)
* [Node.js](http://nodejs.org/)
* [npm](https://www.npmjs.org/)
* [Bower](http://bower.io/)
* [gulp](http://gulpjs.com/)

## Installing
Install the project dependencies using:

    composer install
    npm install
    bower install

## Running Tests
* PHPSpec tests can be run using `bin/phpspec run`
* Behat tests can be run using `bin/behat`

## Running a server
A server can be started using `gulp server`. This will start both a livereload server as well
as a server for the Symfony application. A simple Symfony application server can be started
using `bin/symfony server:run` or `gulp symfony`.

## Loading data
This example project uses a simple SQLite database. To load the database schema use
`bin/symfony doctrine:migrations:migrate` and then `bin/symfony doctrine:fixtures:load` to
load example data into the database.

## Task
As you can see on the static index page (start a server and then visit `http://localhost:8080/`)
there is a carousel at the top of the page (it doesn't work yet) and below it a list of images. Every
image (under the title *Stacks*) is actually a list of one or more images.

When clicking on a stack image, the stack it represents should be inserted into the carousel above.
When the page is first loaded, the first stack should be inserted in the carousel.

In the carousel, the arrows should be hidden if there is no way to move the carousel in that direction.
Note that this means that the carousel should not be infinitely scrolling: it should just stop at the
start and end of the stack.

Make sure the stack name is inserted in the two places where a title can be inserted.
