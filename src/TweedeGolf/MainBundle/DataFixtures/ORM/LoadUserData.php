<?php

namespace TweedeGolf\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $user_manager = $this->container->get('fos_user.user_manager');

        $super_admin = $user_manager->createUser();
        $super_admin->setUsername('superadmin');
        $super_admin->setPlainPassword('superadmin');
        $super_admin->setEmail('superadmin@example.com');
        $super_admin->setEnabled(true);
        $super_admin->addRole('ROLE_SUPER_ADMIN');
        $user_manager->updateUser($super_admin);
        $this->setReference('user.super_admin', $super_admin);

        $admin = $user_manager->createUser();
        $admin->setUsername('admin');
        $admin->setPlainPassword('admin');
        $admin->setEmail('admin@example.com');
        $admin->setEnabled(true);
        $admin->addRole('ROLE_ADMIN');
        $user_manager->updateUser($admin);
        $this->setReference('user.admin', $admin);

        $user = $user_manager->createUser();
        $user->setUsername('user');
        $user->setPlainPassword('user');
        $user->setEmail('user@example.com');
        $user->setEnabled(true);
        $user->addRole('ROLE_USER');
        $user_manager->updateUser($user);
        $this->setReference('user.user', $user);
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 50;
    }
}
