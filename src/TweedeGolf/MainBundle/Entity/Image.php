<?php

namespace TweedeGolf\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="TweedeGolf\MainBundle\Entity\Repository\ImageRepository")
 * @ORM\Table("images")
 * @Vich\Uploadable()
 */
class Image
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank()
     */
    private $filename;

    /**
     * @var File
     *
     * @Vich\UploadableField(fileNameProperty="filename", mapping="files")
     * @Assert\Image()
     */
    private $file;

    /**
     * @var Stack
     *
     * @ORM\ManyToOne(targetEntity="TweedeGolf\MainBundle\Entity\Stack", inversedBy="images")
     * @Assert\NotNull()
     */
    private $stack;

    /**
     * @param Stack $stack
     */
    public function __construct(Stack $stack)
    {
        $this->setStack($stack);
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return Stack
     */
    public function getStack()
    {
        return $this->stack;
    }

    /**
     * @param string $filename
     * @return $this
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * @param File $file
     * @return $this
     */
    public function setFile(File $file)
    {
        $this->file = $file;
        $this->setUpdatedAt(new \DateTime());

        return $this;
    }

    /**
     * @param Stack $stack
     * @return $this
     */
    public function setStack(Stack $stack)
    {
        $this->stack = $stack;

        return $this;
    }
}
